import json
import boto3
from io import StringIO


def save_to_s3(filename, contents):
    # Saves the text payload to S3
    s3 = boto3.resource('s3')
    target_bucket = 'kbisho22-project2'
    target_file = 'data/' + filename + '.txt'
    with open('/tmp/%s.txt' %filename, 'w') as data:
        data.write(contents)
    s3.Bucket(target_bucket).upload_file('/tmp/%s.txt' %filename, target_file, ExtraArgs={'ACL':'public-read'})

def lambda_handler(event, context):
    # SQS triggers this lambda and passes the event and context
    for record in event['Records']:
       print ("test")
       payload=record["body"]
       message_id = record["messageId"]
       save_to_s3(message_id, payload)
       print(str(payload), str(message_id))
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
