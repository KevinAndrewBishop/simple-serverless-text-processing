# simple-serverless-text-processing

Serverless Text Processing App for a Cloud Computing Class

`index.html`: a static website hosted on S3. POSTs user data to an API gateway.

`lamdba01_word_frequency.py`: a lambda function to perform word frequency analysis synchronously and submits the data to an SQS queue.

`lambda01_sqs_to_s3.py` an asynchronous lambda function that pulls data from sqs and saves it to S3.
