import json
import boto3
import datetime

def lambda_handler(event, context):
    """
    Performs simple character frequency analysis on the user provided data.
    """
    response = send_to_sqs(event, context)
    message_id = response['MessageId']
    char_freq = dict()
    for char in event.get('message', '').upper():
        if char != " ":
            char_freq[char] = char_freq.get(char, 0) + 1
    char_freq = sorted(char_freq.items(), key=lambda x: x[1], reverse=True)
    output = ""
    for letter, freq in char_freq:
        output += letter + " " + str(freq) + "&#13;&#10;"
    return {
        'statusCode': 200,
        'body': json.dumps(char_freq),
        'sqs_message_id': message_id
    }

def send_to_sqs(event, context):
    """
    Add the raw text to SQS for further processing.
    """
    sqs = boto3.client('sqs')
    queue_url = 'https://sqs.us-east-2.amazonaws.com/975501589263/kbisho22_image_queue'
    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
    response = sqs.send_message(
        QueueUrl=queue_url,
        DelaySeconds=10,
        MessageAttributes={
            'Datetime': {
                'DataType': 'String',
                'StringValue': now
            },
        },
        MessageBody=(
            event.get('message', '')
        )
    )
    return response
